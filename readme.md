

# Game Ideas

-   Procedurally generated rogue like rpg.
-   Heavily focused on choice and consequence.
    -   Within an individual run and between runs.
-   Turn based combat that is optional.
-   Stealth, persuasion, etc. are alternatives to combat.
-   Non generic fantasy setting.
-   Magic exists and is very powerful and inscrutable.
-   Decisions can have large permanent effects on the world.
-   Character quest lines that branch and can play out over several runs.
-   Single playable character. (not party based).
-   2D graphics, top down view.

