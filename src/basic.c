#include "basic.h"

void _panic(const char *file, int line, const char *format, ...)
{
    // I deliberately do not call va_end here as the program is closing and therefore the OS will cleanup memory
    fprintf(stderr, "Program paniced at: %s:%d\n", file, line);
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    exit(1);
}
