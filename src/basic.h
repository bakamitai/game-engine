#ifndef BASIC
#define BASIC

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>

// Type aliases
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint32_t b32;

typedef float f32;
typedef double f64;

#define true 1
#define false 0

// Funtion to handle fatal errors, prints the message and exits
#define panic(format, ...) _panic(__FILE__, __LINE__, (format) __VA_OPT__(,) __VA_ARGS__)
void _panic(const char *file, int line, const char *format, ...);

// Maths macros
#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define abs(a)    ((a) < 0 ? -(a) : (a))
#define float_to_i32(a) ((i32) ((a) + 0.5))

#endif
