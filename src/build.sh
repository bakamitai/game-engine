#!/bin/sh

if [ "$1" = "--release" ]; then
    FLAGS='-O3'
else
    FLAGS='-Wall -g -DDEBUG'
fi

[ -d "../target" ] || mkdir ../target
cd ../target
rm *
echo "Compiling: basic.c"
cc $FLAGS -c ../src/basic.c
echo "Compiling: image.c"
cc $FLAGS -c ../src/image.c
echo "Compiling: linear_algebra.c"
cc $FLAGS -c ../src/linear_algebra.c
echo "Compiling: game.c"
cc $FLAGS -c ../src/game.c
echo "Compiling: render.c"
cc $FLAGS -c ../src/render.c
echo "Compiling: linux.c"
cc $FLAGS -c ../src/linux.c
cc $FLAGS -lX11 -lm -o game_engine linux.o render.o linear_algebra.o game.o basic.o image.o
