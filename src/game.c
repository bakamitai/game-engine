#include "game.h"
/* #include <stdio.h> */
extern b32 FLASH;
extern b32 wave_attack_active;
extern const u32 wave_attack_width;
extern i32 wave_attack_x;

#define diagonal_unit_dist 0.7071067811865476
static Vec2 dup = { diagonal_unit_dist, -diagonal_unit_dist };
static Vec2 ddown = { diagonal_unit_dist, diagonal_unit_dist };
static Vec2 dforward = { 1.0, 0.0 };
#undef diagonal_unit_dist

BulletList bullet_list_new(u32 size)
{
    BulletList list;
    list.bullets = (Bullet *) calloc(size, sizeof(Bullet));
    list.size = size;
    list.active_count = 0;

    return list;
}

void bullet_list_add(BulletList *list, Vec2 position, f32 speed, Vec2 direction)
{
    if(list->active_count >= list->size) return;
    Bullet *b = list->bullets + list->active_count++;
    b->position = position;
    b->velocity = vec2_scalar_mul(direction, speed);
}

void move_bullets(BulletList *list, i32 width, i32 height)
{
    for(int i = 0; i < list->active_count; ++i)
    {
        Bullet *b = list->bullets + i;
        if(b->position.x < 0 || b->position.x >= width || b->position.y < 0 || b->position.y >= height)
        {
            memmove((void *) (list->bullets + i), (void *) (list->bullets + i + 1), (list->active_count - (i + 1)) * sizeof(Bullet));
            list->active_count--;
            i--;
        }
        else
        {
            b->position.x += b->velocity.x;
            b->position.y += b->velocity.y;
        }
    }
}

static f32 triangle_area(Vec2 a, Vec2 b, Vec2 c)
{
    return abs((a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2.0);
}

b32 collision_check_ship(Ship *ship, BulletList *list)
{
    if(wave_attack_active && ship->head.x >= wave_attack_x && ship->head.x < wave_attack_x + wave_attack_width)
    {
        ship->health--;
        FLASH = 1;
        return true;
    }
    
    for(int i = 0; i < list->active_count; i++)
    {
        Vec2 bullet_pos = list->bullets[i].position;
        f32 area1 = triangle_area(ship->head, ship->left, bullet_pos);
        f32 area2 = triangle_area(ship->head, ship->right, bullet_pos);
        f32 area3 = triangle_area(ship->left, ship->right, bullet_pos);
        f32 ship_area = triangle_area(ship->head, ship->left, ship->right);

        if(abs(ship_area - (area1 + area2 + area3)) < 0.001)
        {
            memmove((void *) (list->bullets + i), (void *) (list->bullets + i + 1), (list->active_count - (i + 1)) * sizeof(Bullet));
            list->active_count--;

            ship->health--;
            FLASH = 1;
            return true;
        }
    }
    return false;
}

void move_ship(Ship *ship, Vec2 direction, i32 width, i32 height)
{
    ship->head = vec2_add(ship->head, direction);
    ship->left = vec2_add(ship->left, direction);
    ship->right = vec2_add(ship->right, direction);

    // Don't go out of bounds buddy
    if(ship->head.x >= width || ship->left.x < 0 || ship->right.y < 0 || ship->left.y >= height)
    {
        direction = vec2_scalar_mul(direction, -1.0);
        ship->head = vec2_add(ship->head, direction);
        ship->left = vec2_add(ship->left, direction);
        ship->right = vec2_add(ship->right, direction);
    }
}

void ship_shoot(Ship ship, BulletList *bullets, u64 current_time)
{
    static u64 previous_call = 0;
    u64 time_diff = current_time - previous_call;
    if(time_diff < 200)
        return;

    switch(ship.mode)
    {
        case FORWARD: bullet_list_add(bullets, ship.head, 6.0, dforward); break;
        case UP:      bullet_list_add(bullets, ship.head, 6.0, dup); break;
        case DOWN:    bullet_list_add(bullets, ship.head, 6.0, ddown); break;
    }

    previous_call = current_time;
}

void collision_check_demons(DemonList *demons, BulletList *list)
{
    for(int i = 0; i < list->active_count; i++)
    {
        Bullet b = list->bullets[i];

        for(int j = 0; j < demons->size; ++j)
        {
            Demon *d = demons->demons + j;
            f32 dx = d->x - b.position.x;
            f32 dy = d->y - b.position.y;
            f32 distance = dx * dx + dy * dy;

            if(distance < d->radius * d->radius)
            {
                memmove((void *) (list->bullets + i), (void *) (list->bullets + i + 1), (list->active_count - (i + 1)) * sizeof(Bullet));
                list->active_count--;
                i--;
                
                d->health--;
                if(d->health == 0)
                {
                    memmove((void *) (demons->demons + j), (void *) (demons->demons + j + 1), (demons->size - (j + 1)) * sizeof(Demon));
                    demons->size--;
                    j--;
                }
            }
        }
    }
}

void demon_list_add(DemonList *demons, Demon d)
{
    if(demons->size < demons->capacity)
    {
        demons->demons[demons->size++] = d;
    }
}

void move_demons(DemonList *demons, i32 width, i32 height)
{
    for(int i = 0; i < demons->size; i++)
    {
        Demon *d = demons->demons + i;
        if(d->x + d->radius <= 0 || d->x - d->radius >= width || d->y + d->radius <= 0 || d->y - d->radius >= height)
        {
            memmove((void *) (demons->demons + i), (void *) (demons->demons + i + 1), (demons->size - (i + 1)) * sizeof(Demon));
            demons->size--;
            i--;
        }
        switch(d->movement)
        {
            case SIN:
            {
                d->x -= 1.0;
                d->y = d->centre + 45.0 * sin(0.05 * d->x);
            } break;

            case COS:
            {
                d->x -= 1.0;
                d->y = d->centre + 45.0 * cos(0.05 * d->x);
            } break;

            case DIAGONAL_DOWN:
            {
                d->x -= 1;
                d->y += 1;
            } break;

            case DIAGONAL_UP:
            {
                d->x -= 1;
                d->y -= 1;
            } break;
        }
    }
}

void demons_shoot(DemonList *demons, BulletList *bullets, u64 current_time)
{
    static u64 previous_call = 0;
    if(current_time - previous_call < 1000l)
        return;
    
    for(int i = 0; i < demons->size; i++)
    {
        Vec2 position = { demons->demons[i].x, demons->demons[i].y };
        Vec2 velocity = { -1.0, 0.0 };
        bullet_list_add(bullets, position, 6.0, velocity);
    }
    previous_call = current_time;
}
