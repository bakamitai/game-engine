#ifndef GAME
#define GAME

#include <stdlib.h>
#include <string.h>
#include "linear_algebra.h"
#include "image.h"
#include "basic.h"

typedef enum
{
    UP,
    FORWARD,
    DOWN,
} ShootMode;

typedef enum
{
    SIN,
    COS,
    DIAGONAL_UP,
    DIAGONAL_DOWN
} MovementType;

typedef struct
{
    Vec2 head;
    Vec2 left;
    Vec2 right;
    f32 length;
    f32 direction;
    ShootMode mode;
    i32 health;
} Ship;

typedef struct
{
    Vec2 position;
    Vec2 velocity;
} Bullet;

typedef struct
{
    u32 size;
    u32 active_count;
    Bullet *bullets;
} BulletList;

typedef struct
{
    f32 x;
    f32 y;
    f32 magnitude;
    f32 radius;
} GravityWell;

typedef struct
{
    f32 x;
    f32 y;
    f32 centre;
    f32 radius;
    u8 health;
    MovementType movement;
} Demon;

typedef struct
{
    f32 x;
    f32 y;
    i32 width;
    i32 height;
    f32 angle;
    u8 health;
} Freak;

typedef enum
{
    VERTICAL,
    HORIZONTAL,
} BeamOrientation;

typedef enum
{
    bFORWARD,
    bBACKWARD,
} BeamDirection;

typedef struct
{
    i32 length;
    i32 countdown_base;
    i32 countdown_current;
    BeamOrientation orientation;
    BeamDirection dir;
} Beam;

typedef struct
{
    Demon *demons;
    u32 size;
    u32 capacity;
} DemonList;

BulletList bullet_list_new(u32 size);
void bullet_list_add(BulletList *list, Vec2 position, f32 speed, Vec2 direction);
void move_bullets(BulletList *list, i32 width, i32 height);
void move_ship(Ship *t, Vec2 direction, i32 width, i32 height);
void ship_shoot(Ship ship, BulletList *bullets, u64 current_time);
void collision_check_demons(DemonList *demons, BulletList *list);
void demon_list_add(DemonList *demons, Demon d);
void move_demons(DemonList *demons, i32 width, i32 height);
b32 collision_check_ship(Ship *ship, BulletList *list);
void demons_shoot(DemonList *demons, BulletList *bullets, u64 current_time);

#endif
