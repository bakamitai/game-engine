#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include "image.h"

typedef enum
{
    BI_RGB = 0,
    BI_RLE8 = 1,
    BI_RLE4 = 2,
    BI_BITFIELDS = 3,
    BI_JPEG = 4,
    BI_PNG = 5,
    BI_ALPHABITFIELDS = 6,
    BI_CMYK = 11,
    BI_CMYKRLE8 = 12,
    BI_CMYKRLE4 = 13,
} CompressionMethod;

int load_bmp(char *filename, Image *out_image)
{
    struct stat file_info;

    if(stat(filename, &file_info) == 0)
    {
        FILE *f = fopen(filename, "r");
        u64 buffer_length = file_info.st_size;
        u8 *buffer = (u8 *) malloc(buffer_length);

        u64 bytes_read = fread(buffer, 1, buffer_length, f);
        fclose(f);

        if(bytes_read == buffer_length)
        {
            i32 width, height, offset, compression;
            i16 color_depth;

            /* two byte int containing color depth of bitmap is 28 bytes into the file */
            color_depth = *((i16 *) (buffer + 28));

            /* four byte int containing compression method used in the bitmap is 30 bytes into the file */
            compression = *((i32 *) (buffer + 30));

            /* four byte int containing the byte offset of the bitmap data is 10 bytes into the file */
            offset = *((i32 *) (buffer + 10));

            /* width and height are 4 byte ints and are at byte offsets 18 and 22 */
            width = *((i32 *) (buffer + 18));
            height = *((i32 *) (buffer + 22));

            /* I deliberately don't free buffer here because the program is being closed and therefore the OS will clean up memory */
            if(buffer[0] != 'B' || buffer[1] != 'M') panic("Invalid file format in file %s\n", filename);   
            if(color_depth != 24) panic("Invalid color depth in bitmap %s, expected 24 bit color depth\n", filename);
            if(compression != BI_RGB) panic("Invalid compression method in bitmap %s\n", filename);

            /* rows in the bitmap data are padded to be multiples of 4 bytes long */
            i32 row_size = 4 * ((color_depth * width + 31) / 32);
            i32 useful_bytes = width * color_depth / 8;
            i32 padding = row_size - useful_bytes;

            u8 *bitmap_data = buffer + offset;
            u8 *image_data = (u8 *) malloc(width * height * 4);
            u32 *int_data = (u32 *) image_data;

            for(i32 y = height - 1; y >= 0; --y)
            {
                for(i32 x = 0; x < width; ++x)
                {
                    u32 pixel = 0;
                    u8 *byte = (u8 *) &pixel;
                    /* extract rgb values the last byte of pixel is padding*/
                    *byte++ = *bitmap_data++;
                    *byte++ = *bitmap_data++;
                    *byte++ = *bitmap_data++;

                    int_data[x + width * y] = pixel;
                }
                /* padding is at the end of the row */
                bitmap_data += padding;
            }
            out_image->data = image_data;
            out_image->width = width;
            out_image->height = height;
        }
        free(buffer);
        return 1;
    }
    else
    {
        panic("IO error in file: %s\n\t%s\n", filename, strerror(errno));
    }
    return 0;
}
