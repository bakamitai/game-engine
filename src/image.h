#include "basic.h"

#ifndef IMAGE
#define IMAGE

typedef struct
{
    i32 width;
    i32 height;
    u8 *data;
} Image;

int load_bmp(char *filename, Image *out_image);

#endif
