#include "linear_algebra.h"

Vec2 vec2_add(Vec2 a, Vec2 b)
{
    Vec2 c = { a.x + b.x, a.y + b.y };
    return c;
}

f64 vec2_get_angle(Vec2 a, Vec2 b)
{
    f64 dot_product = a.x * b.x + a.y * b.y;
    f64 magnitude = sqrt((a.x * a.x + a.y * a.y) * (b.x * b.x + b.y * b.y));

    return acos(dot_product / magnitude);
}

Vec2 vec2_scalar_mul(Vec2 a, f32 scalar)
{
    Vec2 scaled_a = { a.x * scalar, a.y * scalar };
    return scaled_a;
}
