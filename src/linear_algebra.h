#include <math.h>
#include "basic.h"

#ifndef LINEAR_ALGEBRA
#define LINEAR_ALGEBRA

typedef struct
{
    f32 x;
    f32 y;
} Vec2;

Vec2 vec2_add(Vec2 a, Vec2 b);
f64 vec2_get_angle(Vec2 a, Vec2 b);
Vec2 vec2_scalar_mul(Vec2 a, f32 scalar);

#endif
