#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "image.h"
#include "render.h"
#include "linear_algebra.h"
#include "game.h"
#include "basic.h"

#define BLACK  0
#define WHITE  (~0)
#define RED    ((u32) (255 << 16))
#define GREEN  ((u32) (255 << 8))
#define BLUE   ((u32) (255 << 0))
#define CYAN   (BLUE  | GREEN)
#define PURPLE (BLUE  | RED)
#define YELLOW (GREEN | RED)

#define deref_cast(T, A) *((T *) (A))

static f32 pi = 3.14159;
static u32 window_width = 1280;
static u32 window_height = 720;
static u32 offscreen_buffer = 100;
static u32 visual_x = 100;
static u32 visual_y = 100;

static KeySym UP_BUTTON;
static KeySym DOWN_BUTTON;
static KeySym LEFT_BUTTON;
static KeySym RIGHT_BUTTON;
static KeySym DODGE_BUTTON;
static KeySym SHOOT_BUTTON;
b32 FLASH = 0;
i32 wave_attack_x = 0;
const u32 wave_attack_width = 0;
b32 wave_attack_active = 1;

static u32 health_colors_demon[3] = {BLUE, GREEN, RED};

#ifdef DEBUG
static u64 max_duration = 0;
static u64 min_duration = 99999;
#endif

u8 *base;
u8 *waves;

static void quit()
{
#ifdef DEBUG
    printf("max time in millis = %lu\nmin time in millis = %lu\n", max_duration, min_duration);
#endif
    exit(0);
}

KeySym parse_key(char *key_str)
{
    if(!strcmp(key_str, "space\n")) return XK_space;
    if(!strcmp(key_str, "right\n")) return XK_Right;
    if(!strcmp(key_str, "left\n")) return XK_Left;
    if(!strcmp(key_str, "up\n")) return XK_Up;
    if(!strcmp(key_str, "down\n")) return XK_Down;

    char key = *key_str;
    // key format is case insensitive
    if(key >= 'A' && key <= 'Z') key += 32;

    switch(key)
    {
        case 'a': return XK_a;
        case 'b': return XK_b;
        case 'c': return XK_c;
        case 'd': return XK_d;
        case 'e': return XK_e;
        case 'f': return XK_f;
        case 'g': return XK_g;
        case 'h': return XK_h;
        case 'i': return XK_i;
        case 'j': return XK_j;
        case 'k': return XK_k;
        case 'l': return XK_l;
        case 'm': return XK_m;
        case 'n': return XK_n;
        case 'o': return XK_o;
        case 'p': return XK_p;
        case 'q': return XK_q;
        case 'r': return XK_r;
        case 's': return XK_s;
        case 't': return XK_t;
        case 'u': return XK_u;
        case 'v': return XK_v;
        case 'w': return XK_w;
        case 'x': return XK_x;
        case 'y': return XK_y;
        case 'z': return XK_z;
    }
    return 0;
}

b32 load_keybinds()
{
    FILE *bindings = fopen("bindings.txt", "r");

    if(bindings)
    {
        #define buffer_size 128
        char line_buffer[buffer_size];
        char *up_str = "UP_BUTTON";
        char *down_str = "DOWN_BUTTON";
        char *right_str = "RIGHT_BUTTON";
        char *left_str = "LEFT_BUTTON";
        char *dodge_str = "DODGE_BUTTON";
        char *shoot_str = "SHOOT_BUTTON";

        int line_num = 1;
        while(!feof(bindings))
        {
            fgets(line_buffer, buffer_size, bindings);
            char *line = line_buffer;

            // Eat leading whitespace
            for(; *line == '\t' || *line == ' '; line++);

            // Skip comments and empty lines.
            if(*line == '\n' || *line == '#')
            {
                line_num++;
                continue;
            } 
            b32 error_occured = false;

            if(!strncmp(line, up_str, strlen(up_str)))
                error_occured = !(UP_BUTTON = parse_key(line + strlen(up_str) + 3));

            else if(!strncmp(line, down_str, strlen(down_str)))
                error_occured = !(DOWN_BUTTON = parse_key(line + strlen(down_str) + 3));

            else if(!strncmp(line, right_str, strlen(right_str)))
                error_occured = !(RIGHT_BUTTON = parse_key(line + strlen(right_str) + 3));

            else if(!strncmp(line, left_str, strlen(left_str)))
                error_occured = !(LEFT_BUTTON = parse_key(line + strlen(left_str) + 3));

            else if(!strncmp(line, dodge_str, strlen(dodge_str)))
                error_occured = !(DODGE_BUTTON = parse_key(line + strlen(dodge_str) + 3));

            else if(!strncmp(line, shoot_str, strlen(shoot_str)))
                error_occured = !(SHOOT_BUTTON = parse_key(line + strlen(shoot_str) + 3));

            else
                error_occured = true;

            if(error_occured)
            {
                printf("Error in line %d of bindings.txt:\n  %s", line_num, line);
                fclose(bindings);
                return false;
            }
            line_num++;
        }
        fclose(bindings);
        return true;
    }
    printf("Warning: bindings.txt not found.\n");
    return false;
}

void draw_freak(Freak f, Image *img)
{
    fill_rect_unchecked(float_to_i32(f.x) + visual_x, float_to_i32(f.y) + visual_y, f.width / 2, f.height, YELLOW, img);
    fill_rect_unchecked(float_to_i32(f.x) + f.width / 2 + visual_x, float_to_i32(f.y) + visual_y, f.width / 2, f.height, PURPLE, img);
}

void freak_fire(Freak *f, BulletList *list)
{
    Vec2 position = { f->x + f->width / 2, f->y + f->height / 2 };

    f32 angle = f->angle;
    for(int i = 0; i < 4; ++i)
    {
        Vec2 direction = { cos(angle), sin(angle) };
        bullet_list_add(list, position, 6.0, direction);
        angle += M_PI / 2.0;
    }
    f->angle += 0.03;
}

void draw_bullets(BulletList list, u32 color, Image *img)
{
    for(int i = 0; i < list.active_count; i++)
    {
        Bullet b = list.bullets[i];
        rect_unchecked(float_to_i32(b.position.x) + visual_x, float_to_i32(b.position.y) + visual_y, 5, 5, color, img);
    }
}

void draw_health_bar(i32 health, Image *img, u64 current_time)
{
    i32 length = 32;
    i32 gap = 6;
    i32 x = 6 + visual_x;
    i32 y = 6 + visual_y;
    for(int i = 0; i < health; ++i)
    {
        fill_rect_unchecked(x, y, length, length, ~0, img);
        x += length + gap;
    }
    static u64 flash_start = 0;

    if(FLASH && flash_start == 0)
    {
        flash_start = current_time;
    }
    else if(FLASH && current_time - flash_start > 200)
    {
        FLASH = 0;
        flash_start = 0;
    }

    if(FLASH)
    {
        f32 alpha = 1.0 - ((f32) (current_time - flash_start) / 200.0);
        fill_rect_unchecked(x, y, length, length, blend_color(RED, 0, alpha), img);
    }
}

void apply_gravity(GravityWell w, BulletList list)
{
    for(int i = 0; i < list.active_count; ++i)
    {
        Bullet *b = list.bullets + i;
        
        Vec2 direction = { .x = w.x - b->position.x, .y = w.y - b->position.y };

        // Normalize direction vector
        f32 dir_length_squared = direction.x * direction.x + direction.y * direction.y;
        f32 dir_length = sqrt(dir_length_squared);
        if(dir_length > w.radius)
        {
            direction.x = (w.magnitude / dir_length_squared) * direction.x;
            direction.y = (w.magnitude / dir_length_squared) * direction.y;

            b->velocity.x += direction.x;
            b->velocity.y += direction.y;
        }
        else
        {
            b->position.x = w.x - (direction.x / dir_length) * w.radius;
            b->position.y = w.y - (direction.y / dir_length) * w.radius;
            f64 theta = -2 * vec2_get_angle(b->velocity, direction);
            f32 x = b->velocity.x;
            f32 y = b->velocity.y;
            b->velocity.x = x * cos(theta) - y * sin(theta);
            b->velocity.y = x * sin(theta) + y * cos(theta);
        }
    }
}

b32 wave_attack(Beam *beam, u64 delta_time, Image *img)
{
    beam->countdown_current -= delta_time;
    if(beam->countdown_current <= 0) return false;

    f32 window_percent = (f32) beam->countdown_current / (f32) beam->countdown_base;
    i32 beam_x = float_to_i32((f32) window_width * window_percent);
    fill_rect_unchecked(beam_x + visual_x, visual_y, beam->length, window_height, PURPLE, img);
    return true;
    
    /* static u64 start_time = 0; */
    /* if(start_time == 0) */
    /*     start_time = current_time; */

    /* f32 window_percent = (f32) (current_time - start_time) / 5000.0; */
    /* if(window_percent >= 1.0) */
    /* { */
    /*     start_time = 0; */
    /*     return false; */
    /* } */
    
    /* wave_attack_x = float_to_i32((f32) window_width - (f32) window_width * window_percent); */
    /* return true; */
}

u64 time_milliseconds()
{
    struct timespec tp = {0};
    clock_gettime(CLOCK_REALTIME, &tp);
    return tp.tv_sec * 1000 + tp.tv_nsec / 1000000;
}

u64 time_nanoseconds()
{
    struct timespec tp = {0};
    clock_gettime(CLOCK_REALTIME, &tp);
    return tp.tv_nsec;
}

u8 *add_f32(u8 *dest, f32 src)
{
    deref_cast(f32, dest) = src;
    return dest + sizeof(f32);
}

u8 *add_u8(u8 *dest, u8 src)
{
    *dest = src;
    return dest + sizeof(u8);
}

u8 *add_movement_type(u8 *dest, MovementType src)
{
    deref_cast(MovementType, dest) = src;
    return dest + sizeof(MovementType);
}

void next_wave(DemonList *demons)
{
    u8 size = *waves++;
    for(int i = 0; i < size; ++i)
    {
        f32 x, y, centre;
        MovementType type;
        
        x = deref_cast(f32, waves);
        waves += sizeof(f32);

        y = deref_cast(f32, waves);
        waves += sizeof(f32);

        centre = deref_cast(f32, waves);
        waves += sizeof(f32);

        type = deref_cast(MovementType, waves);
        waves += sizeof(MovementType);

        Demon d = {.x = x, .y = y, .radius = 30.0, .centre = centre, .health = 3, .movement = type};
        demon_list_add(demons, d);
    }
}

void draw_text(i32 x, i32 y, char *text, Image font, Image target)
{
    i32 char_width = font.width / 36;
    i32 char_height = font.height;
    char cur;
    u32 *font_pixel = (u32 *) font.data;
    u32 *target_pixel = (u32 *) target.data;

    while((cur = *text++))
    {
        i32 x_offset;
        if(cur >= 'A' && cur <= 'Z')
            x_offset = (cur - 'A') * char_width;
        else if(cur >= '0' && cur <= '9')
            x_offset = (cur - '0' + 26) * char_width;
        
        for(int rows = 0; rows < char_height; ++rows)
        {
            for(int cols = 0; cols < char_width; ++cols)
            {
                target_pixel[x + cols + (y + rows) * target.width] = font_pixel[x_offset + cols + rows * font.width];
            }
        }
        x += char_width;
    }
}

b32 dodge(Ship *ship, Vec2 direction, u64 current_time, Image *img)
{
    f32 dodge_dist = 75.0;
    i32 trail_num = 20;
    static u64 start_time = 0;
    if(start_time == 0)
        start_time = current_time;
    
    f32 dodge_percent = (f32) (current_time - start_time) / 100.0;
    if(dodge_percent >= 1.0)
    {
        start_time = 0;
        move_ship(ship, vec2_scalar_mul(direction, dodge_dist), window_width, window_height);
        return false;
    }

    f32 trail_increment = dodge_percent / (f32) trail_num;
    f32 trail_percent = 0.0;
    f32 alpha = 0.9;
    for(int i = 0; i < trail_num; ++i)
    {
        Vec2 displacement = vec2_scalar_mul(direction, trail_percent * dodge_dist);
        Vec2 head = vec2_add(ship->head, displacement);
        Vec2 left = vec2_add(ship->left, displacement);
        Vec2 right = vec2_add(ship->right, displacement);

        triangle(head.x + visual_x, head.y + visual_y,
                 left.x + visual_x, left.y + visual_y,
                 right.x + visual_x, right.y + visual_y,
                 blend_color(CYAN, BLACK, alpha), img);
        trail_percent += trail_increment;
    }
    return true;
}

int main(int argc, char **argv)
{
    Display *display = XOpenDisplay(NULL);
    unsigned int screen = DefaultScreen(display);
    Window window;
    GC gc;
    XImage *window_buffer;
    XSetWindowAttributes window_attributes = {
        .override_redirect = 1,
        .background_pixel = BLACK,
        .border_pixel = (255 << 8) | (255 << 16),
        .event_mask = StructureNotifyMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | EnterWindowMask | LeaveWindowMask,
    };

    u32 display_width = DisplayWidth(display, screen);
    u32 display_height = DisplayHeight(display, screen);

    window = XCreateWindow(display, RootWindow(display, screen),
                           (display_width - window_width) / 2, (display_height - window_height) / 2,
                           window_width, window_height,
                           2, DefaultDepth(display, screen),
                           InputOutput, DefaultVisual(display, screen),
                           CWBackPixel | CWBorderPixel | CWOverrideRedirect | CWEventMask,
                           &window_attributes);
    XMapWindow(display, window);

    gc = XCreateGC(display, window, 0, NULL);

    window_buffer = XCreateImage(display, DefaultVisual(display, screen),
                                 24, ZPixmap, 0,
                                 (char *) malloc((window_width + offscreen_buffer * 2) * (window_height + offscreen_buffer * 2) * sizeof(u32)),
                                 window_width + offscreen_buffer * 2, window_height + offscreen_buffer * 2, 32, (window_width + offscreen_buffer * 2) * sizeof(u32));
    memset((void *) window_buffer->data, 0, window_width * window_height * sizeof(u32));

    Image window_image = { .width = window_buffer->width, .height = window_buffer->height, .data = (u8 *) window_buffer->data };
    int moving_up = 0;
    int moving_down = 0;
    int moving_right = 0;
    int moving_left = 0;
    int shooting = 0;
    if(!load_keybinds())
    {
        UP_BUTTON = XK_w;
        LEFT_BUTTON = XK_a;
        DOWN_BUTTON = XK_s;
        RIGHT_BUTTON = XK_d;
        SHOOT_BUTTON = XK_space;
        DODGE_BUTTON = XK_j;
    }

    /* Image font; */
    /* load_bmp("font.bmp", &font); */
    /* printf("width = %d, height = %d, character_width = %d\n", font.width, font.height, font.width / 36); */
    /* return 0; */

    XEvent event;

    Ship t = { .head = { 100, 100 }, .length = 40, .direction = pi, .mode = FORWARD, .health = 3 };
    t.left.x = t.head.x + t.length * cos(t.direction - 0.3926991);
    t.left.y = t.head.y + t.length * sin(t.direction - 0.3926991);
    t.right.x = t.head.x + t.length * cos(t.direction + 0.3926991);
    t.right.y = t.head.y + t.length * sin(t.direction + 0.3926991);

    BulletList ship_blist = bullet_list_new(30);
    BulletList demon_blist = bullet_list_new(300);

    base = (u8 *) calloc(1, 200);
    waves = base;
    u8 *local_waves = waves;

    // Wave one
    local_waves = add_u8(local_waves, 4);

    // Demon one
    local_waves = add_f32(local_waves, 690.0);
    local_waves = add_f32(local_waves, 450.0);
    local_waves = add_f32(local_waves, 100.0);
    local_waves = add_movement_type(local_waves, DIAGONAL_UP);

    // Demon two
    local_waves = add_f32(local_waves, 690.0);
    local_waves = add_f32(local_waves, 30.0);
    local_waves = add_f32(local_waves, 180.0);
    local_waves = add_movement_type(local_waves, DIAGONAL_DOWN);

    // Demon three
    local_waves = add_f32(local_waves, 500.0);
    local_waves = add_f32(local_waves, 260.0);
    local_waves = add_f32(local_waves, 260.0);
    local_waves = add_movement_type(local_waves, COS);

    // Demon four
    local_waves = add_f32(local_waves, 500.0);
    local_waves = add_f32(local_waves, 340.0);
    local_waves = add_f32(local_waves, 340.0);
    local_waves = add_movement_type(local_waves, SIN);

    // Wave two
    local_waves = add_u8(local_waves, 1);

    // Demon one
    local_waves = add_f32(local_waves, 700.0);
    local_waves = add_f32(local_waves, 240.0);
    local_waves = add_f32(local_waves, 240.0);
    local_waves = add_movement_type(local_waves, SIN);

    Demon *_d = malloc(4 * sizeof(Demon));
    Freak f = { .x = 500, .y = 300, .width = 60, .height = 50, .angle = 0 };
    Beam beam = { .length = 50, .countdown_base = 5000, .countdown_current = 5000, .orientation = VERTICAL, .dir = bBACKWARD };

    DemonList d = { _d, 0, 4 };
    next_wave(&d);

    int simulation_repeat = 1;
    struct timespec req = {0};
    req.tv_nsec = 16666666l;

    Vec2 left = { -3.0, 0.0 };
    Vec2 right = { 3.0, 0.0 };
    Vec2 up = { 0.0, -3.0 };
    Vec2 down = { 0.0, 3.0 };
    Vec2 dodge_dir = {0};
    b32 dodging = 0;

    int num_waves = 2;
    int current_wave = 1;
    i32 damage_immunity_timer = 0;
    u64 milli_time = 0;
    u64 last_milli_time = time_milliseconds(); // This is important for delta_milli_time to be correct
    u64 delta_milli_time = 0;
    for(;;)
    {
        u64 tm = time_nanoseconds();
#ifdef DEBUG
        u64 tarm = time_milliseconds();
#endif

        // Input
        int queue_size = XPending(display);
        for(int i = 0; i < queue_size; ++i)
        {
            XNextEvent(display, &event);

            switch(event.type)
            {
                case ButtonPress:
                {
                    XUngrabKeyboard(display, CurrentTime);
                    /* free(font.data); */
                    /* free(ship_blist.bullets); */
                    /* free(demon_blist.bullets); */
                    /* free(d.demons); */
                    /* free(base); */
                    /* XDestroyImage(window_buffer); */
                    /* XFreeGC(display, gc); */
                    /* XCloseDisplay(display); */
                    quit();
                } break;
            
                case EnterNotify: XGrabKeyboard(display, window, 1, GrabModeAsync, GrabModeAsync, CurrentTime); break;
                case LeaveNotify: XUngrabKeyboard(display, CurrentTime); break;

                case KeyPress:
                {
                    KeySym symbol = XLookupKeysym(&event.xkey, 0);
                    if(symbol == UP_BUTTON) moving_up = 1;
                    if(symbol == DOWN_BUTTON) moving_down = 1;
                    if(symbol == LEFT_BUTTON) moving_left = 1;
                    if(symbol == RIGHT_BUTTON) moving_right = 1;
                    if(symbol == SHOOT_BUTTON) shooting = 1;
                    if(symbol == DODGE_BUTTON)
                    {
                        dodge_dir.x = moving_right - moving_left;
                        dodge_dir.y = moving_down - moving_up;
                        if(dodge_dir.x != 0 && dodge_dir.y != 0)
                        {
                            dodge_dir = vec2_scalar_mul(dodge_dir, 0.7071067811865476);
                        }
                        dodging = 1;
                    }

                    switch(symbol)
                    {
                        /* case UP_BUTTON: moving_up = 1; break; */
                        /* case LEFT_BUTTON: moving_left = 1; break; */
                        /* case DOWN_BUTTON: moving_down = 1; break; */
                        /* case RIGHT_BUTTON: moving_right = 1; break; */
                        case XK_z: t.mode = UP; break;
                        case XK_x: t.mode = FORWARD; break;
                        case XK_c: t.mode = DOWN; break;
                        /* case DODGE_BUTTON: */
                        /* { */
                        /*     dodge_dir.x = moving_right - moving_left; */
                        /*     dodge_dir.y = moving_down - moving_up; */
                        /*     if(dodge_dir.x != 0 && dodge_dir.y != 0) */
                        /*     { */
                        /*         dodge_dir = vec2_scalar_mul(dodge_dir, 0.7071067811865476); */
                        /*     } */
                        /*     dodging = 1; */
                        /* } break; */
                        /* case SHOOT_BUTTON: shooting = 1; break; */

                        case XK_Escape:
                        {
                            XUngrabKeyboard(display, CurrentTime);
                            quit();
                        } break;
                    }
                } break;

                case KeyRelease:
                {
                    KeySym symbol = XLookupKeysym(&event.xkey, 0);
                    if(symbol == UP_BUTTON) moving_up = 0;
                    if(symbol == DOWN_BUTTON) moving_down = 0;
                    if(symbol == LEFT_BUTTON) moving_left = 0;
                    if(symbol == RIGHT_BUTTON) moving_right = 0;
                    if(symbol == SHOOT_BUTTON) shooting = 0;
                    /* switch(symbol) */
                    /* { */
                    /*     case UP_BUTTON: moving_up = 0; break; */
                    /*     case LEFT_BUTTON: moving_left = 0; break; */
                    /*     case DOWN_BUTTON: moving_down = 0; break; */
                    /*     case RIGHT_BUTTON: moving_right = 0; break; */
                    /*     case SHOOT_BUTTON: shooting = 0; break; */
                    /* } */
                } break;
            }
        }

        // Simulate
        milli_time = time_milliseconds();
        delta_milli_time = milli_time - last_milli_time;
        for(int i = 0; i < simulation_repeat; i++)
        {
            if(!dodging)
            {
                if(moving_up) move_ship(&t, up, window_width, window_height);
                if(moving_down) move_ship(&t, down, window_width, window_height);
                if(moving_left) move_ship(&t, left, window_width, window_height);
                if(moving_right) move_ship(&t, right, window_width, window_height);
                if(shooting) ship_shoot(t, &ship_blist, milli_time);
            }
            move_bullets(&ship_blist, window_width, window_height);
            move_bullets(&demon_blist, window_width, window_height);
            move_demons(&d, window_width, window_height);
            collision_check_demons(&d, &ship_blist);
            demons_shoot(&d, &demon_blist, milli_time);
            freak_fire(&f, &demon_blist);

            if(!dodging && damage_immunity_timer <= 0)
            {
                if(collision_check_ship(&t, &demon_blist))   
                {
                    damage_immunity_timer = 1000;
                }
            }
            else if(damage_immunity_timer > 0) damage_immunity_timer -= delta_milli_time;

            if(t.health <= 0)
            {
                printf("YOU DIED!\n");
                quit();
            }
            if(d.size == 0)
            {
                if(current_wave <= num_waves)
                {
                    current_wave++;
                    next_wave(&d);
                }
            }
        }

        // Render
        fill_rect_unchecked(visual_x, visual_y, window_width, window_height, 0, &window_image);
        if(dodging) dodging = dodge(&t, dodge_dir, milli_time, &window_image);
        else
        {
            triangle(t.head.x + visual_x, t.head.y + visual_y,
                     t.left.x + visual_x, t.left.y + visual_y,
                     t.right.x + visual_x, t.right.y + visual_y,
                     CYAN, &window_image);
        }
        draw_bullets(ship_blist, YELLOW, &window_image);
        draw_bullets(demon_blist, PURPLE, &window_image);
        for(int i = 0; i < d.size; i++)
        {
            Demon dee = d.demons[i];
            circle(dee.x + visual_x, dee.y + visual_y, dee.radius, health_colors_demon[dee.health - 1], &window_image);
        }
        if(wave_attack_active) wave_attack_active = wave_attack(&beam, delta_milli_time, &window_image);
        draw_freak(f, &window_image);
        draw_health_bar(t.health, &window_image, milli_time);

        /* draw_text(150 + visual_x, 10 + visual_y, "SPACE911", font, window_image); */

        XPutImage(display, window, gc, window_buffer,
                  visual_x, visual_y, 0, 0,
                  window_width, window_height);
        last_milli_time = milli_time;

#ifdef DEBUG
        tarm = time_milliseconds() - tarm;
        if(tarm > max_duration) max_duration = tarm;
        if(tarm < min_duration) min_duration = tarm;
#endif

        tm = time_nanoseconds() - tm;
        req.tv_nsec = 16666666l - tm;
        nanosleep(&req, NULL);
    }
    return 0;
}
