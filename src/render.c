#include "basic.h"
#include "image.h"
#include "render.h"
/* #include <stdio.h> */

u32 blend_color(u32 color1, u32 color2, f32 alpha)
{
    u32 blend;
    u8 *channel1 = (u8 *) &color1;
    u8 *channel2 = (u8 *) &color2;
    u8 *channel_blend = (u8 *) &blend;

    *channel_blend = (u8) (alpha * ((f32) *channel1) + (1 - alpha) * ((f32) *channel2) + 0.5);
    channel_blend++;
    channel1++;
    channel2++;

    *channel_blend = (u8) (alpha * ((f32) *channel1) + (1 - alpha) * ((f32) *channel2) + 0.5);
    channel_blend++;
    channel1++;
    channel2++;

    *channel_blend = (u8) (alpha * ((f32) *channel1) + (1 - alpha) * ((f32) *channel2) + 0.5);

    return blend;
}

void clear_image(Image *target, u32 color)
{
    u32 *int_data = (u32 *) target->data;
    for(int i = 0; i < target->width * target->height; ++i)
    {
        /* int_data[i] = blend_color(color, int_data[i], 0.1); */
        int_data[i] = color;
    }
}

void fill_rect_unchecked(i32 x, i32 y, i32 width, i32 height, u32 color, Image *img)
{
    u32 *data = (u32 *) img->data;
    data += x + y * img->width;

    for(int i = 0; i < height; ++i)
    {
        for(int j = 0; j < width; ++j)
        {
            data[j] = color;
        }
        data += img->width;
    }
}

// TODO: rewrite line drawing functions to not use pixel function
void pixel(i32 x, i32 y, u32 color_pixel, Image *img)
{
    if(x < 0 || x >= img->width || y < 0 || y >= img->height) return;

    u32 *address = ((u32 *) img->data) + x + y * img->width;
    *address = color_pixel;
}

void circle(f32 x, f32 y, f32 radius, u32 color, Image *img)
{
    i32 centre_x = (i32) (x + 0.5);
    i32 centre_y = (i32) (y + 0.5);
    i32 int_radius = (i32) (radius + 0.5);
    i32 square_radius = (i32) (radius * radius + 0.5);
    u32 *data = (u32 *) img->data;

    for(i32 i = -int_radius; i <= 0; ++i)
    {
        for(i32 j = -int_radius; j <= 0; ++j)
        {
            if(j * j + i * i <= square_radius)
            {
                /* pixel(centre_x + i, centre_y + j, color, img); */
                /* pixel(centre_x - i, centre_y + j, color, img); */
                /* pixel(centre_x + i, centre_y - j, color, img); */
                /* pixel(centre_x - i, centre_y - j, color, img); */

                data[centre_x + i + (centre_y + j) * img->width] = color;
                data[centre_x - i + (centre_y + j) * img->width] = color;
                data[centre_x + i + (centre_y - j) * img->width] = color;
                data[centre_x - i + (centre_y - j) * img->width] = color;
            }
        }
    }
}

void line(i32 x1, i32 y1, i32 x2, i32 y2, u32 color_pixel, Image *img)
{
    /* if((x1 < 0 && x2 < 0) || (x1 >= img->width && x2 >= img->width) || */
    /*    (y1 < 0 && y2 < 0) || (y1 >= img->height && y2 >= img->height)) */
    /*     return; */
    
    // Bresenham method, only uses integer arithmetic. That's pretty good
    i32 dx = abs(x2 - x1);
    i32 sx = x1 < x2 ? 1 : -1;
    i32 dy = -abs(y2 - y1);
    i32 sy = y1 < y2 ? 1 : -1;
    i32 err = dx + dy;
    u32 *data = (u32 *) img->data + x1 + y1 * img->width;

    while(x1 != x2 || y1 != y2)
    {
        /* pixel(x1, y1, color_pixel, img); */
        *data = color_pixel;
        i32 e2 = 2 * err;
        if(e2 >= dy)
        {
            err += dy;
            x1 += sx;
            data += sx;
        }
        if(e2 <= dx)
        {
            err += dx;
            y1 += sy;
            data += sy * img->width;
        }
    }
}
void rect_unchecked(i32 x, i32 y, i32 width, i32 height, u32 color_pixel, Image *img)
{
    u32 img_width = img->width;
    u32 idx1 = y * img_width + x;
    u32 idx2 = idx1 + img_width * (height - 1);
    u32 idx3 = y * img_width + x;
    u32 idx4 = idx3 + (width - 1);
    u32 *data = (u32 *) img->data;

    // Draw horizontal lines
    for(u32 i = 0; i < width; ++i)
    {
        data[idx1++] = color_pixel;
        data[idx2++] = color_pixel;
    }

    // Draw vertical lines
    for(u32 i = 0; i < height; ++i)
    {
        data[idx3] = color_pixel;
        data[idx4] = color_pixel;
        idx3 += img_width;
        idx4 += img_width;
    }
}

#define LEFT    (1 << 0)
#define RIGHT   (1 << 1)
#define TOP     (1 << 2)
#define BOTTOM  (1 << 3)
void rect(i32 x, i32 y, i32 width, i32 height, u32 color_pixel, Image *img)
{
    // Safety precautions
    // A bit being set in sides indicates that the side should be drawn as it is inside the visible area
    u8 sides = LEFT | RIGHT | TOP | BOTTOM;
    if(x >= img->width || x <= -width || y >= img->height || y <= -height) return;

    if(x < 0)
    {
        width += x;
        x = 0;
        sides &= ~LEFT;
    }
    if(y < 0)
    {
        height += y;
        y = 0;
        sides &= ~TOP;
    }
    if(x + width >= img->width) 
    {
        width = img->width - x;
        sides &= ~RIGHT;
    }
    if(y + height >= img->height)
    {
        height = img->height - y;
        sides &= ~BOTTOM;
    }

    u32 *data = (u32 *) img->data;


    u32 idx1 = y * img->width + x;
    u32 idx2 = idx1 + img->width * height;
    u32 idx3 = y * img->width + x;
    u32 idx4 = idx3 + width;
    u32 img_width = img->width;

    // All sides visible
    if(sides == (LEFT | RIGHT | TOP | BOTTOM))
    {
        // Note: There need to be two separate loops because I want this to work for
        // all rectangles if it just drew squares it could be one loop.
        
        // Draw horizontal lines
        for(u32 i = 0; i < width; ++i)
        {
            data[idx1++] = color_pixel;
            data[idx2++] = color_pixel;
        }

        // Draw vertical lines
        for(u32 i = 0; i < height; ++i)
        {
            data[idx3] = color_pixel;
            data[idx4] = color_pixel;
            idx3 += img_width;
            idx4 += img_width;
        }
    }
    else 
    {
        // Draw left side
        if((sides & LEFT))
        {
            for(u32 i = 0; i < height; ++i)
            {
                data[idx3] = color_pixel;
                idx3 += img_width;
            }
        }

        // Draw right side
        if((sides & RIGHT))
        {
            for(u32 i = 0; i < height; ++i)
            {
                data[idx4] = color_pixel;
                idx4 += img_width;
            }
        }

        // Draw top side
        if((sides & TOP))
        {
            for(u32 i = 0; i < width; ++i)
            {
                data[idx1++] = color_pixel;
            }
        }

        // Draw bottom side
        if((sides & BOTTOM))
        {
            for(u32 i = 0; i < width; ++i)
            {
                data[idx2++] = color_pixel;
            }
        }
    }
}

void triangle(i32 x1, i32 y1, i32 x2, i32 y2, i32 x3, i32 y3, u32 color, Image *img)
{
    line(x1, y1, x2, y2, color, img);
    line(x1, y1, x3, y3, color, img);
    line(x2, y2, x3, y3, color, img);
}

void shape(Vec2 *vertices, int vertex_num, u32 color, Image *img)
{
    for(int i = 0; i < vertex_num - 1; i++)
    {
        Vec2 v1 = vertices[i];
        Vec2 v2 = vertices[i + 1];
        line(float_to_i32(v1.x), float_to_i32(v1.y), float_to_i32(v2.x), float_to_i32(v2.y), color, img);
    }

    // Connect last vertex to first vertex
    Vec2 v1 = vertices[0];
    Vec2 v2 = vertices[vertex_num - 1];
    line(float_to_i32(v1.x), float_to_i32(v1.y), float_to_i32(v2.x), float_to_i32(v2.y), color, img);
}
