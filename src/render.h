#include "image.h"
#include "linear_algebra.h"
#include "basic.h"
#ifndef RENDER
#define RENDER

void clear_image(Image *target, u32 color);
u32 blend_color(u32 color1, u32 color2, f32 alpha);
void fill_rect_unchecked(i32 x, i32 y, i32 width, i32 height, u32 color, Image *img);
void pixel(i32 x, i32 y, u32 color_pixel, Image *image);
void line(i32 x1, i32 y1, i32 x2, i32 y2, u32 color_pixel, Image *img);
void rect_unchecked(i32 x, i32 y, i32 width, i32 height, u32 color_pixel, Image *img);
void rect(i32 x, i32 y, i32 width, i32 height, u32 color_pixel, Image *image);
void circle(f32 x, f32 y, f32 radius, u32 color, Image *img);
void triangle(i32 x1, i32 y1, i32 x2, i32 y2, i32 x3, i32 y3, u32 color, Image *img);
void shape(Vec2 *vertices, int vertex_num, u32 color, Image *img);

#endif
